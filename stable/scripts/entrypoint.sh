#!/bin/bash
set -ux

steamcmd +runscript $PWD/update_zomboid.cmd
chown -R $RUN_AS $PZ_HOME
su - "$RUN_AS" -c "\"${PZ_HOME}/start-server.sh\" $*"
