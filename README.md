<h4 align="center">
  <img alt="Zomboid Banner" src="assets/banner.jpg">
</h4>

---

# Zomboid Server Docker Image

[![Gitlab stars](https://img.shields.io/gitlab/stars/hiiamlala/zomboid-server-docker)](https://gitlab.com/hiiamlala/zomboid-server-docker)
[![Gitlab forks](https://img.shields.io/gitlab/forks/hiiamlala/zomboid-server-docker)](https://gitlab.com/hiiamlala/zomboid-server-docker)
[![Gitlab last-commit](https://img.shields.io/gitlab/last-commit/hiiamlala/zomboid-server-docker)](https://gitlab.com/hiiamlala/zomboid-server-docker)
[![Gitlab open-issues](https://img.shields.io/gitlab/issues/open/hiiamlala/zomboid-server-docker)](https://gitlab.com/hiiamlala/zomboid-server-docker)
[![Gitlab pipeline-status](https://img.shields.io/gitlab/pipeline-status/hiiamlala/zomboid-server-docker)](https://gitlab.com/hiiamlala/zomboid-server-docker)

[![Docker Pulls](https://badgen.net/docker/pulls/hiiamlala/zomboid-server?icon=docker&label=pulls)](https://hub.docker.com/r/hiiamlala/zomboid-server/)
[![Docker Stars](https://badgen.net/docker/stars/hiiamlala/zomboid-server?icon=docker&label=stars)](https://hub.docker.com/r/hiiamlala/zomboid-server/)
[![Docker Image Size](https://badgen.net/docker/size/hiiamlala/zomboid-server?icon=docker&label=image%20size)](https://hub.docker.com/r/hiiamlala/zomboid-server/)

This is a minimum image based on debian:11-slim with some extra packages to be able to run **Project Zomboid Dedicated Server**\
\
I've just follow this [document](https://pzwiki.net/wiki/Dedicated_server) from pzwiki to make it work.

# Usage

### Prerequisities

In order to run this container you'll need docker installed.

- [Windows](https://docs.docker.com/windows/started)
- [OS X](https://docs.docker.com/mac/started/)
- [Linux](https://docs.docker.com/linux/started/)

Create directories for game data and game installation.

```shell
mkdir -p ./Zomboid
mkdir -p ./pzserver
chown 1210:1210 ./Zomboid
chown 1210:1210 ./pzserver
```

### To run a fresh server

#### With Docker

_Interactive_

```shell
docker run \
  -n zomboid-server \
  -v ./Zomboid:/home/pzuser/Zomboid \
  -v ./pzserver:/opt/pzserver \
  -p 16261:16261/udp \
  -p 16262:16262/udp \
  -it \
  hiiamlala/zomboid-server:latest
```

_Detach_

```shell
docker run -n zomboid-server \
  -v ./Zomboid:/home/pzuser/Zomboid \
  -v ./pzserver:/opt/pzserver \
  -p 16261:16261/udp \
  -p 16262:16262/udp \
  -d \
  hiiamlala/zomboid-server:latest
```

#### With Docker Compose

Take a look at my docker-compose.yaml, configure as you need then run.

```shell
docker compose up -d
```

### To run existed server data

Instead of creating new Zomboid directory then just copy your Zomboid directory to it. Change owner to **uid 1210** with command.

```shell
chown 1210:1210 ./Zomboid
```

_Note that you still need **./pzserver** directory for the game installation._

```shell
mkdir -p ./pzserver
chown 1210:1210 ./pzserver
```

And then just follow those commands above to start it.

### Stop/Delete the server

#### With Docker

_Interactive_

```shell
quit
```

_Detach_

```shell
docker stop zomboid-server

# Or deletion
docker rm zomboid-server -f
```

#### With Docker Compose

Take a look at my docker-compose.yaml, configure as you need then run.

```shell
docker compose stop

# Or deletion
docker compose down -v
```

# How to access it

The server will listen on 2 ports:

- **16261/udp**
- **16262/udp**

So you also need to make sure that you have access to the server on those ports by adding it to firewall whitelist follow this [Instruction](https://pzwiki.net/wiki/Dedicated_server#Forwarding_required_ports)

Or if you run it on a server behind NAT, you've to config port forwarding by your self.

If every thing is fine then follow the [document](https://pzwiki.net/wiki/Dedicated_server#Connecting_to_the_server) to join it from your game client.

# Modify server configuration

Take a look at this
[Document](https://pzwiki.net/wiki/Dedicated_server#Administrating_the_server)

# Mods installation

Take a look at this
[Document](https://pzwiki.net/wiki/Dedicated_server#Installing_mods)

# Troubleshoot

Take a look at this first
[Document](https://pzwiki.net/wiki/Dedicated_server#Troubleshooting)

If it doesn't help, feel free to raise an issue :D

# Credits

Thanks to PZ Community then let's "Axe" some zombies

# See Also

Minecraft Dedicated Server [MC Server](https://gitlab.com/hiiamlala/minecraft-server-docker)!

# License

ISC
