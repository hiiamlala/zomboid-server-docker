variable "DOCKER_USER" {}

variable "DOCKER_IMAGE" {}

group "default" {
  targets = ["stable", "beta"]
}

target "stable" {
  context = "stable"
  dockerfile = "Dockerfile"
  tags = ["${DOCKER_USER}/${DOCKER_IMAGE}:latest"]
}

target "beta" {
  context = "beta"
  dockerfile = "Dockerfile"
  tags = ["${DOCKER_USER}/${DOCKER_IMAGE}:beta"]
}
